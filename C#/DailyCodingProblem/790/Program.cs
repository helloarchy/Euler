﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace _790
{
    /**
     * This problem was asked by Pivotal.
     * A step word is formed by taking a given word, adding a letter, and anagramming the result.
     * For example, starting with the word "APPLE", you can add an "A" and anagram to get "APPEAL".
     * Given a dictionary of words and an input word, create a function that returns all valid step words.
     */
    class Program
    {
        static void Main(string[] args)
        {
            var dictionary = new ArrayList
            {
                "APPLE",
                "APPEAL",
                "TESTWORD"
            };

            const string word = "APPLE";

            Console.WriteLine("Step words are:");
            foreach (var step in GetStepWords(dictionary, word))
            {
                Console.WriteLine(step);
            }
        }

        private static ArrayList GetStepWords(ArrayList dictionary, string word)
        {
            var stepWords = new ArrayList();

            // Word + A
            word += "a";

            // Alphabetise
            var letters = word.ToUpper().ToCharArray();
            Array.Sort(letters);
            var alphabetisedWord = new string(letters);
            foreach (string dictionaryWord in dictionary)
            {
                // Alphabetise dictionary words too
                letters = dictionaryWord.ToUpper().ToCharArray();
                Array.Sort(letters);
                var alphabetisedDictionaryWord = new string(letters);
                if (alphabetisedWord.Equals(alphabetisedDictionaryWord))
                {
                    // If match, add to list
                    stepWords.Add(dictionaryWord);
                }
            }

            // Return list
            return stepWords;
        }
    }
}